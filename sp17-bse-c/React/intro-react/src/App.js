import React, { Component } from "react";

import Movies from "./components/movies";

class App extends Component {
  state = {
    movies: [
      {
        title: "Gladiator"
      },
      {
        title: "Troy"
      }
    ]
  };
  render() {
    return (
      <div className="App">
        <Movies />
      </div>
    );
  }
}

export default App;
