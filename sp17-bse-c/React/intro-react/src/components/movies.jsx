import React, { Component } from "react";
class Movies extends Component {
  state = {
    movies: []
  };
  render() {
    return (
      <div className="movies">
        {this.state.movies.length === 0 && <h3>There are no movies</h3>}
        <h3>List All Movies</h3>
        {this.state.movies.map(m => (
          <div key={m.title}>{m.title}</div>
        ))}
      </div>
    );
  }
  componentDidMount() {
    fetch("https://usman-vidly.herokuapp.com/api/movies")
      .then(response => response.json())
      .then(data => this.setState({ movies: data }));
  }
}

export default Movies;
