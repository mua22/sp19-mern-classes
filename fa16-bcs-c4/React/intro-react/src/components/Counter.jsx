import React, { Component } from "react";
class Counter extends Component {
  state = {};
  render() {
    return (
      <div>
        Counter Component
        <h1>{this.props.name}</h1>
      </div>
    );
  }
}

export default Counter;
