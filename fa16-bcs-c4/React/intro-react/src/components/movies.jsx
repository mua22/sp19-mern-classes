import React, { Component } from "react";
import Axios from "axios";

class Movies extends Component {
  state = {
    moviesData: []
  };
  handleSort = () => {
    this.setState({ moviesData: this.state.moviesData.sort() });
  };
  componentDidMount() {
    Axios.get("https://usman-vidly.herokuapp.com/api/movies").then(res => {
      console.log(res);
      this.setState({ moviesData: res.data });
      //alert(res);
    });
  }
  render() {
    return (
      <div>
        <h4>All Moviess</h4>
        <button onClick={this.handleSort}>Sort Movies</button>
        {this.state.moviesData.length == 0 && <p>No Movies</p>}
        <ul>
          {this.state.moviesData.map(m => (
            <li key={m.title}>{m.title}</li>
          ))}
          {/* <li>{this.state.moviesData[0]}</li>
          <li>{this.state.moviesData[1]}</li>
          <li>{this.state.moviesData[2]}</li> */}
          <li />
        </ul>
      </div>
    );
  }
}

export default Movies;
