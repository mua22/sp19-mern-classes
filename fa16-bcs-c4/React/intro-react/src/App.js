import React, { Component } from "react";
import Counter from "./components/Counter";
import Student from "./components/Student";
import { Faculty, Staff } from "./components/employes";
import Movies from "./components/movies";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Movies />
        <Counter name="Class Counter" />
        <Student />
        <Faculty />
        <Staff />
      </div>
    );
  }
}

export default App;
